var getUser = (id, callback) => {
  var user = {
    id: id,
    name: 'Mike'
  }

  setTimeout(() => {
    callback(user);
  }, 1000);
}

getUser(333, (userObj) => {
  console.log(userObj);
});