var somePromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('It worked!');
  }, 2000);
})

somePromise.then((msg) => {
  console.log('Success: ', msg);
}, (errorMsg) => {
  console.log('Error: ', msg);
});