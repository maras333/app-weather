const yargs = require('yargs');
const geocode = require('./geocode/geocode.js');
const weather = require('./weather/weather.js');

const argv = yargs
  .options({
    a: {
      demandOption: true,
      describe: 'Address to fetch weather for',
      alias: 'address',
      string: true
    }
  })
  .help()
  .alias('help', 'h')
  .argv;

geocode.geocodeAddress(argv.address)
  .then((geolocationResult) => {
    return weather.fetchWeather(geolocationResult.latitude, geolocationResult.longitude)
  })
  .then((weatherResult) => {
    console.log(`It's currently: ${weatherResult.currentlyTemp}. It feels like ${weatherResult.currentlyApparentTemp}`);
  })
  .catch(e => console.log(e));