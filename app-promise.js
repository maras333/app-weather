const yargs = require('yargs');
const axios = require('axios');
const fs = require('fs');

const argv = yargs
  .options({
    a: {
      demandOption: true,
      describe: 'Address to fetch weather for',
      alias: 'address',
      string: true
    }
  })
  .help()
  .alias('help', 'h')
  .argv;

let encodedAddress = encodeURIComponent(argv.a);

axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}&key=AIzaSyD_4uwxWDeRaXltySah5EgG6uHfbhQ7IPI`)
  .then((response) => {
    if (response.data.status === 'ZERO_RESULTS') {
      throw new Error('Unable to find address');
    }

    let latitude = response.data.results[0].geometry.location.lat;
    let longitude = response.data.results[0].geometry.location.lng;
    let weatherUrl = `https://api.darksky.net/forecast/d3ea42379c3fe37412ac822170d52937/${latitude},${longitude}`;
    return axios.get(weatherUrl)
  })
  .then((response) => {
    let currentlyTemp = response.data.currently.temperature;
    let currentlyApparentTemp = response.data.currently.apparentTemperature;
    let data = `It's currently: ${currentlyTemp}. It feels like ${currentlyApparentTemp}`;
    fs.appendFile('weatherData.txt', ` \n${data}`, (err) => {
      console.log('here');
      if (err) throw err;
      console.log(`The ${data} was appended to file!`);
    });
  })
  .catch((e) => {
    if (e.code === 'ENOTFOUND') {
      console.log('Unable to connect to darksky api servers');
    } else {
      console.log(e.message);
    }
  });