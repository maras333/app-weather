const request = require('request');

let fetchWeather = (latitude, longitude) => {

  return new Promise((resolve, reject) => {
    request({
      url: `https://api.darksky.net/forecast/d3ea42379c3fe37412ac822170d52937/${latitude},${longitude}`,
      json: true
    }, (error, response, body) => {
      if (error) {
        reject('Unable to connect to darksky api servers');
      } else if (response.statusCode === 404) {
        reject('Bad request');
      } else if (response.statusCode === 200) {
        resolve({
          currentlyTemp: body.currently.temperature,
          currentlyApparentTemp: body.currently.apparentTemperature,
        });
      }
    });
  });
}

module.exports.fetchWeather = fetchWeather;